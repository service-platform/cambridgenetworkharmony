import api from '../api/api.js';

Date.prototype.Format = function (fmt) { //author: meizz
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

const sendFormIdAndGatherFormId = (e) => {
  let formId = e.detail.formId;
  //1.模拟器，不参与收集 formIds
  if (formId == "the formId is a mock one") { return; }

  //发送 formIds 到后端
  console.log("发送：" + formId);
  
  api.saveFormId({
    method: 'POST',
    header: {
      'Content-Type': 'application/json',
      userAuth: wx.getStorageSync("userId"),
      wechatAuth: wx.getStorageSync("openId")
    },
    data: {
      formId: formId
    },
    success: function (data) {
      console.log("formId 已提交到后台。");
    },
    fail: function (err) {
      console.warn("formId 提交失败。");
    }
  });
}
module.exports = {
  sendFormIdAndGatherFormId
}