//app.js
import API from 'api/api.js';
App({
  onLaunch: function () {
    var that = this;
    if (wx.getStorageSync("countryFlag") == ""){
        wx.setStorageSync("countryFlag", false);
    }
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
          var latitude = res.latitude;
          var longitude = res.longitude;

          API.getTheLocation({
            method: 'GET',
            lat: latitude,
            lng: longitude,
            success: function (json) {
                console.log(json)
                if (json.data.results.length <= 0){
                    console.log("当前位置未知");
                    wx.setStorageSync("countryFlag", false);
                }else{
                    var formatted_address = json.data.results[json.data.results.length - 1].formatted_address;

                    console.log("你当前在：" + formatted_address);
                    if (formatted_address == "中国"){
                      wx.setStorageSync("countryFlag", true);
                    } else {
                      wx.setStorageSync("countryFlag", false);
                    }
                }
                getApp().globalData.isLocationOK = true;
            },
            fail: function(){
              console.log("Google 解析失败。");
              wx.setStorageSync("countryFlag", false);
              getApp().globalData.isLocationOK = true;
            }
          });
      },
      fail: function () {
        console.log("定位失败。");
        wx.setStorageSync("countryFlag", false);
        getApp().globalData.isLocationOK = true;
      }
    });
  },
  globalData: {
    isLocationOK: false
  }
})