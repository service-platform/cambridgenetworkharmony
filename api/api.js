// 选择地址位置（Production）
// const host_cn = 'https://www.ciiedu.cn'
// const host_us = 'https://www.ciiedu.net'
// 选择地址位置（Test）
const host_cn = 'http://uat.ciiedu.net/api-app'
const host_us = 'http://uat.ciiedu.net/api-app'
// 选择地址位置（Development）
// const host_cn = 'http://192.168.0.51:9887'
// const host_us = 'http://192.168.0.51:9887'
// 文件获取密码
const fileSecret = 'HX423IX-XJSHO384-JM490MCX';

const wxRequest = (params, url) => {
  if(params.header == undefined){
    params.header = {
      'Content-Type': 'application/json'
    }
  }
  var country_location = "";

  if(url.indexOf("maps.google.cn") == -1){
    // true: 中国，false: 美国
    country_location = host_us + url;
    if (wx.getStorageSync("countryFlag") == true) {
      country_location = host_cn + url;
    }
  }else{
    country_location = url
  }
  wx.request({
    url: country_location,
    method: params.method || 'GET',
    data: params.data || {},
    header: params.header,
    success: (res) => {
      params.success && params.success(res)
    },
    fail: (res) => {
      params.fail && params.fail(res)
    },
    complete: (res) => {
      params.complete && params.complete(res)
    }
  })
}

// 获取地理位置
// http://maps.google.cn/maps/api/geocode/json?latlng=39.86748880822139,-96.2770128250122&language=CN
const getTheLocation = (params) => wxRequest(params, 'https://maps.google.cn/maps/api/geocode/json?key=AIzaSyCZyNR7OlL5cJqNbWPY_LXfN0-SnSlEkSg&latlng=' + params.lat + ',' + params.lng+'&language=CN')
// 判断是否绑定
const whetherBind = (params) => wxRequest(params, '/appAPI/staff/unAuth/whetherBind/' + params.code)
// 授权账户，并登陆
const authorizationRelatedAccount = (params) => wxRequest(params, '/appAPI/staff/authorizationRelatedAccount')
// 查找学生列表(分享页)
const findStudentListForSharing = (params) => wxRequest(params, '/appAPI/dataInteraction/findStudentListForSharing')
// 我的学生列表
const studentList = (params) => wxRequest(params, '/appAPI/dataInteraction/student/list')
// Photo Album List(POST)
const findGeneralPhotoesBySId = (params) => wxRequest(params, '/appAPI/student/findGeneralPhotoesBySId/' + params.sid)
// 删除图片
const removeGeneralPhotoes = (params) => wxRequest(params, '/appAPI/student/removeGeneralPhotoes')
// 查询学生详情
const findStuById = (params) => wxRequest(params, '/appAPI/student/findStuById/' + params.sId)
// 查询个人信息
const findStaffByOpenId = (params) => wxRequest(params, '/appAPI/staff/findStaffByOpenId')
// 修改个人信息
const updateStaffInfo = (params) => wxRequest(params, '/appAPI/staff/updateStaffInfo')
// 报告列表
const reports = (params) => wxRequest(params, '/appAPI/student/view/reports?studentId=' + params.studentId + "&type=" + params.type)
// 报告详情
const reportDetail = (params) => wxRequest(params, '/appAPI/student/view/reportDetail?studentId=' + params.studentId + "&type=" + params.type + "&reportId=" + params.reportId)
// 获取 FEA 和 SSC
const getFEAAndSSC = (params) => wxRequest(params, '/appAPI/student/find/' + params.entityType + '/' + params.entityId + '/' + params.title)
// 分享图片给家长汇
const sharePhotoToParents = (params) => wxRequest(params, '/appAPI/student/sharePhotoToParents')
// 根据报告 ID，查看图片列表
const findReportPhotoesByRId = (params) => wxRequest(params, '/appAPI/student/findReportPhotoesByRId/' + params.rId)
// 根据 ID，删除当月报告图片
const removeReportPhoto = (params) => wxRequest(params, '/appAPI/student/removeReportPhoto')
// 获取分享者
const generalPhotoDetail = (params) => wxRequest(params, '/appAPI/student/generalPhotoDetail/' + params.id)
// 收集客户端 formIds
const saveFormId = (params) => wxRequest(params, '/appAPI/notification/saveFormId')
// Student Notes
const listInterviewInfo = (params) => wxRequest(params, '/appAPI/note/listInterviewInfo/' + params.sId + "?status=POTENTIAL&status=APPLYING&status=ACCEPTED&status=REJECTED&status=DECLINED&status=SELECTED&status=CONDITIONAL_ACCEPTANCE&status=ENROLLED&status=RE_ENROLLED&status=OUT_OF_NETWORK_TRANSFER&status=IN_NETWORK_TRANSFER&status=MANAGED_BY")
// Notes List
const findNotes = (params) => wxRequest(params, '/appAPI/note/findNotes')
// Add Notes
const addNote = (params) => wxRequest(params, '/appAPI/note/addNote/' + params.receiverId + "/" + params.notify)
// find Note Detail
const findNoteDetail = (params) => wxRequest(params, '/appAPI/note/findNoteDetail/' + params.nId)
// add Commment
const addCommment = (params) => wxRequest(params, '/appAPI/note/addCommment')
// get Public Key
const getPublicKey = (params) => wxRequest(params, '/appAPI/staff/common/getpublickey')

module.exports = {
  fileSecret,
  getTheLocation,
  whetherBind,
  authorizationRelatedAccount,
  findStudentListForSharing,
  studentList,
  findGeneralPhotoesBySId,
  findStaffByOpenId,
  updateStaffInfo,
  removeGeneralPhotoes,
  findStuById,
  reports,
  reportDetail,
  getFEAAndSSC,
  sharePhotoToParents,
  findReportPhotoesByRId,
  removeReportPhoto,
  generalPhotoDetail,
  saveFormId,
  listInterviewInfo,
  addNote,
  findNotes,
  findNoteDetail,
  addCommment,
  getPublicKey,
  host_cn,
  host_us
}