import API from '../../api/api';
import utils from '../../utils/util.js';

Page({
  data: {
    timeout: 0,
    contentHeight: '456px',
    imgSource: [],
    type: "monthly",
    searchKey: '',
    description: '',
    studentList: [],
    selectSIds: [],
    selectName: '',
    currentPage: 1,
    totalPage: 0,
    pageSize: 0,
    updateIndex: 1,
    errorIndex: 0,
    timeoutIndex: 0,
    errArray: [],
    timeoutArray: [],

    isLoading: false,
    isLoadingData: false,
    isNetOff: false
  },
  onLoad: function () {
    //获取设备的 高度
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          contentHeight: (res.windowHeight-147) + "px",
          imgWidth: Math.floor((res.windowWidth - 24) / 3) + "px",
          scrollHeight: (res.windowHeight - 53) + "px"
        });
      }
    });
    this.refreshData(this.data.searchKey, 1, 10);
  },
  bindRefreshData: function(){
    this.refreshData(this.data.searchKey, 1, 10);
  },
  bindMonthlyAndGeneral: function(e){
    if (e.target.dataset.flag == "monthly"){
      this.setData({ type: "monthly", isLoadingData: true });
      this.findStudentListForSharing(this.data.searchKey);
    } else if (e.target.dataset.flag == "general") {
      this.setData({ type: "general", isLoadingData: true });
      this.findStudentListForSharing(this.data.searchKey);
    }
  },
  bindInput: function (e) {
    var that = this;
    if (e.target.dataset.flag == "searchKey") {
      this.setData({ searchKey: e.detail.value });

      clearTimeout(that.data.timeout);
      that.data.timeout = setTimeout(function(){
        that.setData({ isLoadingData: true });
        that.findStudentListForSharing(e.detail.value);
      }, 1500);
    } else if (e.target.dataset.flag == "description") {
      this.setData({ description: e.detail.value });
    }
  },
  btnChooseImage: function () {
    var that = this;
    wx.chooseImage({
      count: 9, // 最多选择 5 张
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        var tempFilePaths = res.tempFilePaths; var imageArray = [];
        for (var i = 0; i < tempFilePaths.length; i++){
          imageArray.push({
            src: tempFilePaths[i],
            width: 0
          });
        }
        that.setData({
          imgSource: imageArray
        });
      }
    })
  },
  findStudentListForSharing: function (searchKey) {
    this.data.currentPage = 1;
    this.refreshData(searchKey, 1, (this.data.currentPage * 10));
  },
  bindScrollToLower: function(){
    if (this.data.currentPage < this.data.totalPage){
      this.data.currentPage++;
      this.refreshData(this.data.searchKey, 1, (this.data.currentPage * 10));
    }
  },
  refreshData: function (searchKey, pageNumber, pageSize){
      var that = this;
      var paramData = {
        conditions: {
          monthlyOrGeneral: that.data.type,
          studentName: searchKey
        },
        pageNumber: pageNumber,
        pageSize: pageSize
      };
      if (pageSize <= 10 && that.data.studentList.length <= 0) {
        that.setData({ isLoadingData: true, isNetOff: false, pageSize: pageSize, studentList: [], totalPage: 0 });
      } else {
        that.setData({ pageSize: pageSize });
      }
      API.findStudentListForSharing({
        method: 'POST',
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("userId"),
          wechatAuth: wx.getStorageSync("openId")
        },
        data: paramData,
        success: function (json) {
          if (json.statusCode != 200) {
            that.setData({ isLoadingData: false, isNetOff: true, studentList: [], totalPage: 0, currentPage: that.data.currentPage });
            return;
          }
          if (pageSize == 10){
            that.setData({ studentList: json.data.data.content, totalPage: json.data.data.totalPage, isLoadingData: false, isNetOff: false, currentPage: that.data.currentPage});
          }else{
            that.setData({ studentList: json.data.data.content, isLoadingData: false, isNetOff: false, currentPage: that.data.currentPage});
          }
        },
        fail: function (err) {
          that.data.currentPage--;
          that.setData({ isLoadingData: false, isNetOff: true,currentPage: that.data.currentPage });
          console.log(err);
        }
      })
  },
  bindChange: function (e){
      this.setData({ selectSIds: new Array(e.detail.value.split('|Name|')[0]), selectName: e.detail.value.split('|Name|')[1] });
  },
  uploadImgTask: function(){
      var that = this; var selectSIds = "";
      for (var i = 0; i < this.data.selectSIds.length; i++) {
        selectSIds += this.data.selectSIds[i] + ",";
      }
      selectSIds = selectSIds.substr(0, selectSIds.length - 1);

      that.setData({ isLoading: true }); wx.showLoading({ title: 'Uploading(' + that.data.updateIndex + ')...', mask: true });

      const uploadTask = wx.uploadFile({
        url: (wx.getStorageSync("countryFlag") ? API.host_cn : API.host_us) + '/appAPI/dataInteraction/sharePhotoes/' + this.data.type,
        filePath: this.data.imgSource[this.data.updateIndex - 1].src,
        name: 'sharePhoto',
        header: {
          'Content-Type': 'multipart/form-data',
          userAuth: wx.getStorageSync("userId"),
          wechatAuth: wx.getStorageSync("openId")
        },
        formData: {
          description: this.data.description,
          sOrRIds: selectSIds
        },
        success: function (json) {
          if (json.statusCode == 200){
              var objData = JSON.parse(json.data);

              if (objData.data[0].length <= 0){
                that.data.errArray.push(that.data.updateIndex);
                that.data.errorIndex++;  //错误加一，
                that.data.updateIndex++; //继续下一张
                console.log("Upload failed.");
              }else{
                that.data.updateIndex++;
                console.log("Upload successfully.");
              }
          }else{
              that.data.errArray.push(that.data.updateIndex);
              that.data.errorIndex++;  //错误加一，
              that.data.updateIndex++; //继续下一张
              console.log("Upload failed.");
          }
          console.log(json);
        },
        fail: function (err) {
          if (err.errMsg == "uploadFile:fail 网络连接已中断。" || err.errMsg == "uploadFile:fail 请求超时。" || err.errMsg.toLowerCase().indexOf('timeout') != -1) {
            that.data.timeoutArray.push(that.data.updateIndex);
            that.data.timeoutIndex++;  //错误加一，
            that.data.updateIndex++; //继续下一张
            console.log("Upload timeout.");
          }else{
            that.data.errArray.push(that.data.updateIndex);
            that.data.errorIndex++;  //错误加一，
            that.data.updateIndex++; //继续下一张
            console.log("Upload failed.");
          }
          console.log("Fail:" + err.errMsg);
        },
        complete: function (e) {
          if (that.data.updateIndex > that.data.imgSource.length) {
            that.setData({ isLoading: false }); wx.hideLoading();
            var strContent = "You upload " + that.data.imgSource.length + " photos to " + that.data.selectName + ".\n";
            var errInfo = ""; var timeroutInfo = "";

            for (var x = 0; x < that.data.errArray.length; x++) {
              if (that.data.errArray[x] == 1){
                errInfo += that.data.errArray[x] + "st, ";
              } else if (that.data.errArray[x] == 2) {
                errInfo += that.data.errArray[x] + "nd, ";
              } else if (that.data.errArray[x] == 3) {
                errInfo += that.data.errArray[x] + "rd, ";
              } else {
                errInfo += that.data.errArray[x] + "th, ";
              }
            }
            for (var x = 0; x < that.data.timeoutArray.length; x++) {
              if (that.data.timeoutArray[x] == 1) {
                timeroutInfo += that.data.timeoutArray[x] + "st, ";
              } else if (that.data.timeoutArray[x] == 2) {
                timeroutInfo += that.data.timeoutArray[x] + "nd, ";
              } else if (that.data.timeoutArray[x] == 3) {
                timeroutInfo += that.data.timeoutArray[x] + "rd, ";
              } else {
                timeroutInfo += that.data.timeoutArray[x] + "th, ";
              }
            }
            if (errInfo == "" && timeroutInfo == "") {
              strContent += "Success: " + (that.data.imgSource.length - that.data.errorIndex - that.data.timeoutIndex) + " Photos.\n";
              strContent += "Failure: " + that.data.errorIndex + " Photos.\n";
              strContent = strContent.substr(0, strContent.length - 1);
            } else if (errInfo != "" && timeroutInfo == ""){
              errInfo = errInfo.substr(0, errInfo.length - 2);

              strContent += "Success: " + (that.data.imgSource.length - that.data.errorIndex - that.data.timeoutIndex) + " Photos.\n";
              strContent += "Failure: " + that.data.errorIndex + " Photos.（the " + errInfo + " failed）\n";
              strContent = strContent.substr(0, strContent.length - 1);
            } else if (timeroutInfo != "" && errInfo == "") {
              timeroutInfo = timeroutInfo.substr(0, timeroutInfo.length - 2);

              strContent += "Success: " + (that.data.imgSource.length - that.data.errorIndex - that.data.timeoutIndex) + " Photos.\n";
              strContent += "Connection timed out: " + that.data.timeoutIndex + " Photos.（the " + timeroutInfo + "）\n";
              strContent = strContent.substr(0, strContent.length - 1);
            } else if (timeroutInfo != "" && errInfo != "") {
              errInfo = errInfo.substr(0, errInfo.length - 2);
              timeroutInfo = timeroutInfo.substr(0, timeroutInfo.length - 2);

              strContent += "Success: " + (that.data.imgSource.length - that.data.errorIndex - that.data.timeoutIndex) + " Photos.\n";
              strContent += "Failure: " + that.data.errorIndex + " Photos.（the " + errInfo + " failed）\n";
              strContent += "Connection timed out: " + that.data.timeoutIndex + " Photos.（the " + timeroutInfo + "）\n";
              strContent = strContent.substr(0, strContent.length - 1);
            }

            wx.showModal({
              content: strContent,
              confirmText: 'OK',
              showCancel: false,
              success: function (res) {
                  var studentList = that.data.studentList;
                  that.setData({
                    description: '',
                    studentList: [],
                    selectSIds: [],
                    imgSource: [],
                    updateIndex: 1,
                    errorIndex: 0,
                    timeoutIndex: 0,
                    errArray: [],
                    timeoutArray: [],
                    selectName: ''
                  });
                  that.setData({ studentList: studentList });
              }
            });
          } else {
            //递归调用 uploadImgTask 函数
            that.uploadImgTask();
          }
        }
      });
      if (uploadTask != undefined){
        uploadTask.onProgressUpdate((res) => {
          var tempObj = that.data.imgSource[that.data.updateIndex - 1];
          tempObj.width = parseInt(105 * (res.progress / 100));
          that.setData({ imgSource: that.data.imgSource });
        })
      }
  },
  bindShareInfo: function(){
      if (this.data.imgSource.length <= 0) {
        wx.showModal({
          content: 'Please select at least one picture.',
          confirmText: 'OK',
          showCancel: false
        });
        return;
      }
      if(this.data.selectSIds.length <= 0){
        wx.showModal({
          content: 'Please select 1 student.',
          confirmText: 'OK',
          showCancel: false
        });
        return;
      }

      var that = this;
      wx.showModal({
        content: 'Are you sure to upload ' + this.data.imgSource.length + ' photos for ' + this.data.selectName + ' ?',
        confirmText: 'OK',
        cancelText: 'Cancel',
        success: function (res) {
          if (res.confirm) {
            wx.pageScrollTo({
              scrollTop: 0,
              duration: 300
            });
            that.uploadImgTask();
          }
        }
      });
  },
  bindPreviewPicture: function(e){
    var imgArrays = [];
    for (var i = 0; i < this.data.imgSource.length; i++) {
      imgArrays.push(this.data.imgSource[i].src);
    }

    wx.previewImage({
      current: e.target.dataset.img,
      urls: imgArrays
    })
  },
  bindPreviewImg: function(e){
    if (this.data.type == 'monthly'){
      if(e.currentTarget.dataset.hascurrentmonthreport == true){
        wx.navigateTo({
          url: '/pages/sharePhoto/previewImageList/previewImageList?rId=' + e.currentTarget.dataset.id
        });
      }else{
        wx.showModal({
          content: "Sorry, " + (e.currentTarget.dataset.fullname) + " doesn't have residential report for current month.Please create in Harmony Website firstly. ",
          confirmText: 'OK',
          showCancel: false
        });
      }
    }else{
      wx.navigateTo({
        url: '/pages/sharePhoto/previewImageList/previewImageList?sid=' + e.currentTarget.dataset.id
      });
    }
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
