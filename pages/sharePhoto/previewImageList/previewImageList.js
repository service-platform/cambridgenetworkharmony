import API from '../../../api/api';
import utils from '../../../utils/util.js';

Page({
  data: {
    id: '', //sId or rId
    mask: '',
    serverData: [],
    originalData: [],
    previewCurrentPage: 1,
    previewTotalPage: 0,
    previewPageSize: 0,
    isPreviewLoading: false,
    isPreviewNetOff: false,
    imgWidth: '0px',
    scrollHeight: "0px",
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          imgWidth: Math.floor((res.windowWidth - 24) / 3) + "px",
          scrollHeight: (res.windowHeight - 53) + "px"
        });
      }
    });
    if (options.sid == undefined){
      that.setData({ id: options.rId, mask: 'report' }, function () {
        that.onRefreshData(that.data.id, 1, 20);
      })
    }else{
      that.setData({ id: options.sid, mask: 'student' }, function () {
        that.onRefreshData(that.data.id, 1, 20);
      })
    }
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
    this.onRefreshData(this.data.id, 1, (this.data.previewCurrentPage * 20));
  },
  bindClosePreview: function (e) {
    wx.navigateBack();
  },
  bindPreviewScrollToLower: function () {
    if (this.data.previewCurrentPage < this.data.previewTotalPage) {
      this.data.previewCurrentPage++;
      this.onRefreshData(this.data.id, 1, (this.data.previewCurrentPage * 20));
    }
  },
  bindRefreshDeleteData: function () {
    if (this.data.id != '') {
      this.onRefreshData(this.data.id, 1, (this.data.previewCurrentPage * 20));
    }
  },
  bindPreviewRefreshData: function () {
    this.onRefreshData(this.data.id, 1, 20);
  },
  onRefreshData: function (id, pageNumber, pageSize) {
    var that = this;
    if (pageSize <= 20) {
      that.setData({ isPreviewLoading: true, isPreviewNetOff: false, previewPageSize: pageSize });
    } else {
      that.setData({ previewPageSize: pageSize });
    }
    if (that.data.mask == 'student'){
      API.findGeneralPhotoesBySId({
        method: 'POST',
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("userId"),
          wechatAuth: wx.getStorageSync("openId")
        },
        sid: id,
        data: {
          conditions: {},
          pageNumber: pageNumber,
          pageSize: pageSize,
          dir: 'DESC'
        },
        success: function (json) {
          if (json.statusCode != 200) {
            that.setData({ isPreviewLoading: false, isPreviewNetOff: true, serverData: [], previewTotalPage: 0, previewCurrentPage: that.data.previewCurrentPage });
            return;
          }
          if (json.data.code == 1) {
            var tempArray = [];
            for (var i = 0; i < json.data.data.content.length; i++) {
              var obj = json.data.data.content[i];
              if (tempArray.length <= 0) {
                var objItem = {
                  date: new Date(obj.dateCreated).Format("MM-dd-yyyy"),
                  items: new Array(obj)
                };
                tempArray.push(objItem);
              } else {
                var haveData = false;
                for (var j = 0; j < tempArray.length; j++) {
                  if (new Date(obj.dateCreated).Format("MM-dd-yyyy") == tempArray[j].date) {
                    tempArray[j].items.push(obj);
                    haveData = true; break;
                  }
                }
                if (haveData == false) {
                  var objItem = {
                    date: new Date(obj.dateCreated).Format("MM-dd-yyyy"),
                    items: new Array(obj)
                  };
                  tempArray.push(objItem);
                }
              }
            }
            if (pageSize <= 20) {
              that.setData({ serverData: tempArray, previewTotalPage: json.data.data.totalPage, originalData: json.data.data.content, isPreviewLoading: false, isPreviewNetOff: false, previewCurrentPage: that.data.previewCurrentPage });
            } else {
              that.setData({ serverData: tempArray, originalData: json.data.data.content, isPreviewLoading: false, isPreviewNetOff: false, previewCurrentPage: that.data.previewCurrentPage });
            }
            console.log(tempArray);
          }
        },
        fail: function (err) {
          if (that.data.previewCurrentPage >= 2) { that.data.previewCurrentPage--; }
          that.setData({ isPreviewLoading: false, isPreviewNetOff: true, previewCurrentPage: that.data.previewCurrentPage });
        }
      })
    }else{
      API.findReportPhotoesByRId({
        method: 'POST',
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("userId"),
          wechatAuth: wx.getStorageSync("openId")
        },
        rId: id,
        data: {
          conditions: {},
          pageNumber: pageNumber,
          pageSize: pageSize,
          dir: 'DESC'
        },
        success: function (json) {
          if (json.statusCode != 200) {
            that.setData({ isPreviewLoading: false, isPreviewNetOff: true, serverData: [], previewTotalPage: 0, previewCurrentPage: that.data.previewCurrentPage });
            return;
          }
          if (json.data.code == 1) {
            var tempArray = [];
            for (var i = 0; i < json.data.data.content.length; i++) {
              var obj = json.data.data.content[i];
              if (tempArray.length <= 0) {
                var objItem = {
                  date: new Date(obj.createdOn).Format("MM-dd-yyyy"),
                  items: new Array(obj)
                };
                tempArray.push(objItem);
              } else {
                var haveData = false;
                for (var j = 0; j < tempArray.length; j++) {
                  if (new Date(obj.createdOn).Format("MM-dd-yyyy") == tempArray[j].date) {
                    tempArray[j].items.push(obj);
                    haveData = true; break;
                  }
                }
                if (haveData == false) {
                  var objItem = {
                    date: new Date(obj.createdOn).Format("MM-dd-yyyy"),
                    items: new Array(obj)
                  };
                  tempArray.push(objItem);
                }
              }
            }
            if (pageSize <= 20) {
              that.setData({ serverData: tempArray, previewTotalPage: json.data.data.totalPage, originalData: json.data.data.content, isPreviewLoading: false, isPreviewNetOff: false, previewCurrentPage: that.data.previewCurrentPage });
            } else {
              that.setData({ serverData: tempArray, originalData: json.data.data.content, isPreviewLoading: false, isPreviewNetOff: false, previewCurrentPage: that.data.previewCurrentPage });
            }
            console.log(tempArray);
          }
        },
        fail: function (err) {
          if (that.data.previewCurrentPage >= 2) { that.data.previewCurrentPage--; }
          that.setData({ isPreviewLoading: false, isPreviewNetOff: true, previewCurrentPage: that.data.previewCurrentPage });
        }
      })
    }
  },
  openImage: function (e) {
    if (e.target.dataset.img != "/images/imgError.png") {
      if (e.target.dataset.whethersharetoparent == true && e.target.dataset.whethersee == true) {
        wx.navigateTo({
          url: '/pages/photoAlbum/previewImage/previewImage?img=' + encodeURIComponent(e.target.dataset.img) + "&desc=" + encodeURIComponent(e.target.dataset.description) + "&date=" + encodeURIComponent(e.target.dataset.date) + "&shareDate=" + encodeURIComponent(e.currentTarget.dataset.sharetoparenttime) + "&whethersharetoparent=" + e.currentTarget.dataset.whethersharetoparent + "&sId=" + this.data.id + "&nDId=" + e.target.dataset.ndid + "&btnDisabled=true&mask=" + this.data.mask
        });
      } else {
        wx.navigateTo({
          url: '/pages/photoAlbum/previewImage/previewImage?img=' + encodeURIComponent(e.target.dataset.img) + "&desc=" + encodeURIComponent(e.target.dataset.description) + "&date=" + encodeURIComponent(e.target.dataset.date) + "&shareDate=" + encodeURIComponent(e.currentTarget.dataset.sharetoparenttime) + "&whethersharetoparent=" + e.currentTarget.dataset.whethersharetoparent + "&sId=" + this.data.id + "&nDId=" + e.target.dataset.ndid + "&btnDisabled=false&mask=" + this.data.mask
        });
      }
    }
  },
  showDescription: function (e) {
    if (e.target.dataset.val != "") {
      wx.showModal({
        content: e.target.dataset.val,
        confirmText: 'OK',
        showCancel: false
      });
    }
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})