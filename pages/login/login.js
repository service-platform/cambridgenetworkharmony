import API from '../../api/api';
var RSA = require('../../utils/RSA.js'); 

Page({
  data: {
      isDisabled: false,
      userName: '',
      password: ''
  },
  bindInput: function(e){
    if (e.target.dataset.flag == "userName"){
      this.setData({ userName: e.detail.value});
    } else if (e.target.dataset.flag == "password"){
      this.setData({ password: e.detail.value });
    }
  },
  reauthorization: function () {
      var page = this;
      wx.openSetting({
        success: function (res) {
          if (res.authSetting['scope.userInfo']) {
            wx.showModal({
              content: "Please click again to login!",
              confirmText: 'OK',
              showCancel: false
            });
          }else{
            //未授权，重新授权
            page.reauthorization();
          }
        }
      });
  },
  btnLogin: function(e){
    var that = this;
    if (that.data.userName.trim() == "" || that.data.password.trim() == ""){
      wx.showModal({
        content: 'Please input your username and password.',
        confirmText: 'OK',
        showCancel: false
      });
      return;
    }
    if (e.detail.errMsg == "getUserInfo:fail auth deny"){
      that.reauthorization();
    }else{
      that.setData({ isDisabled: true });
      wx.login({
        success: function (res) {
          if (res.code) {
            API.whetherBind({
              method: "GET",
              code: res.code,
              success: function (json) {
                if (json.statusCode == 200) {
                  if (json.data.data.code == 0) {
                    wx.setStorageSync("openId", json.data.data.openId);

                    //获取用户信息
                    var userInfo = e.detail.userInfo;
                    wx.showLoading({ title: 'Login in', mask: true });

                    API.getPublicKey({
                      header: {
                        'Content-Type': 'application/json',
                        wechatAuth: wx.getStorageSync("openId")
                      },
                      success: function (json) {
                        if (json.data.code == 1) {
                          var publicKey_pkcs1 = '-----BEGIN PUBLIC KEY-----' + json.data.data.publicKey + '-----END PUBLIC KEY-----';
                          var encrypt_rsa = new RSA.RSAKey();
                          encrypt_rsa = RSA.KEYUTIL.getKey(publicKey_pkcs1);
                          var encStr = encrypt_rsa.encrypt(that.data.password);
                          encStr = RSA.hex2b64(encStr);

                          var paramData = {
                            username: that.data.userName,
                            password: encStr,
                            appId: 'weichat',
                            requestId: json.data.data.requestId,

                            nickName: userInfo.nickName,
                            gender: userInfo.gender,
                            city: userInfo.city,
                            province: userInfo.province,
                            country: userInfo.country,
                            avatarUrl: userInfo.avatarUrl
                          }

                          API.authorizationRelatedAccount({
                            method: "POST",
                            header: {
                              'Content-Type': 'application/json',
                              wechatAuth: wx.getStorageSync("openId")
                            },
                            data: paramData,
                            success: function (json) {
                              if (json.statusCode != 200) {
                                wx.hideLoading(); that.setData({ isDisabled: false });
                                if (json.data.includes("Request processing failed; nested exception is creativestars.coruscant.exception.CoruscantException: ") == true) {
                                  var tempStr = json.data.substr(json.data.indexOf("Request processing failed; nested exception is creativestars.coruscant.exception.CoruscantException: "), json.data.length - 1);
                                  tempStr = tempStr.replace("Request processing failed; nested exception is creativestars.coruscant.exception.CoruscantException: ", "");

                                  wx.showModal({
                                    content: tempStr,
                                    confirmText: 'OK',
                                    showCancel: false
                                  });
                                } else {
                                  wx.showModal({
                                    content: "Please connect the network.",
                                    confirmText: 'OK',
                                    showCancel: false
                                  });
                                }
                                return;
                              }

                              if (json.data.data != undefined){
                                wx.hideLoading(); that.setData({ isDisabled: false });
                                if (json.data.data.userId == undefined) {
                                  wx.showModal({
                                    content: json.data.data.msg,
                                    confirmText: 'OK',
                                    showCancel: false
                                  });
                                } else {
                                  wx.setStorageSync("userId", json.data.data.userId);
                                  wx.showModal({
                                    content: json.data.data.msg,
                                    confirmText: 'OK',
                                    showCancel: false,
                                    success: function (res) {
                                      if (res.confirm) {
                                        wx.switchTab({
                                          url: '/pages/home/home'
                                        });
                                      }
                                    }
                                  });
                                }
                              } else if(json.data.data == undefined){
                                wx.hideLoading(); that.setData({ isDisabled: false });
                                wx.showModal({
                                  content: "The email or username you’ve entered doesn’t match any account.",
                                  confirmText: 'OK',
                                  showCancel: false
                                });
                              }
                            },
                            fail: function (err) {
                              wx.hideLoading(); that.setData({ isDisabled: false });
                              wx.showModal({
                                content: "Please connect the network.",
                                confirmText: 'OK',
                                showCancel: false
                              });
                            }
                          });
                        } else {
                          wx.hideLoading(); that.setData({ isDisabled: false });
                          wx.showModal({
                            content: "Please connect the network.",
                            confirmText: 'OK',
                            showCancel: false
                          });
                        }
                      },
                      fail: function (err) {
                        wx.hideLoading(); that.setData({ isDisabled: false });
                        wx.showModal({
                          content: "Please connect the network.",
                          confirmText: 'OK',
                          showCancel: false
                        });
                      }
                    });
                  } else {
                    wx.setStorageSync("openId", json.data.data.openId);
                    wx.setStorageSync("userId", json.data.data.userId);
                    that.setData({ isDisabled: false });

                    wx.showModal({
                      content: 'Successful login.',
                      confirmText: 'OK',
                      showCancel: false,
                      success: function (res) {
                        if (res.confirm) {
                          wx.switchTab({
                            url: '/pages/home/home'
                          });
                        }
                      }
                    });
                  }
                }
              },
              fail: function (err) {
                that.setData({ isDisabled: false });
                wx.showModal({
                  content: "Please connect the network.",
                  confirmText: 'OK',
                  showCancel: false,
                });
              }
            });
          }
        },
        fail: function (err) {
          that.setData({ isDisabled: false });
          wx.showModal({
            content: "Please connect the network.",
            confirmText: 'OK',
            showCancel: false,
          });
        }
      });
    }
  }
})
