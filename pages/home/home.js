import API from '../../api/api';
import utils from '../../utils/util.js';

Page({
  data: {
    displayShow: true,
    articlesCount: 5,
    xOffset: 2000,
    showWhetherBind: false,
    viewHeight: '0px'
  },
  onLoad: function(){
    // var animation = wx.createAnimation({
    //   duration: 500,
    //   timingFunction: 'ease',
    // }).opacity(1).width('100%').height(23).step();

    // this.setData({
    //   animationData: animation.export()
    // });
    var that = this;
    var interval = setInterval(function(){
      if(getApp().globalData.isLocationOK == true){
        that.whetherBind();
        clearInterval(interval);
      }
    }, 500);
    
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          viewHeight: res.windowHeight + "px",
          xOffset: (Math.floor(res.windowWidth / 2) - 23)
        });
      }
    });
  },
  whetherBind: function(){
    var that = this;
    that.setData({ showWhetherBind: false });
    wx.login({
      success: function (res) {
        if (res.code) {
          API.whetherBind({
            method: "GET",
            code: res.code,
            success: function (json) {
              if (json.statusCode == 200) {
                if (json.data.data.code == 0) {
                  wx.setStorageSync("openId", json.data.data.openId);
                  wx.redirectTo({
                    url: '/pages/login/login'
                  });
                } else {
                  wx.setStorageSync("openId", json.data.data.openId);
                  wx.setStorageSync("userId", json.data.data.userId);
                  that.setData({ showWhetherBind: false });
                }
              }else{
                that.setData({ showWhetherBind: true });
              }
            },
            fail: function (err) {
              that.setData({ showWhetherBind: true });
            }
          });
        }
      },
      fail: function (err) {
        that.setData({ showWhetherBind: true });
      }
    });
  },
  btnSharePhoto: function () {
    wx.navigateTo({
      url: '/pages/sharePhoto/sharePhoto'
    })
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})