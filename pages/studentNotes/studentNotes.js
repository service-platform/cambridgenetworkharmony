import API from '../../api/api';

Page({
  data: {
    sId: "",
    englishName: "",
    serverData: [],
    isLoading: false,
    isNetOff: false
  },
  onLoad: function (options) {
    var that = this;
    this.setData({ sId: options.sId, englishName: decodeURIComponent(options.englishName) }, function () {
      wx.setNavigationBarTitle({ title: "(" + that.data.englishName + ") Student Notes" });
      that.onRefreshData(that.data.sId);
    });
  },
  onRefreshData: function (sId) {
    var that = this;
    this.setData({ serverData:[], isLoading: true, isNetOff: false });
    
    API.listInterviewInfo({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      sId: sId,
      success: function (json) {
        if (json.statusCode != 200){
          that.setData({ isLoading: false, isNetOff: true });
          return;
        }
        if (json.data.code == 1){
          that.setData({
            serverData: json.data.data,
            isLoading: false,
            isNetOff: false
          });
        }
      },
      fail: function (err) {
        that.setData({ isLoading: false, isNetOff: true });
      }
    });
  },
  bindRefreshData: function(){
    this.onRefreshData(this.data.sId);
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
    this.onRefreshData(this.data.sId);
  },
  gotoNoteList: function(e){
    if (e.currentTarget.dataset.type == "STUDENT"){
      wx.navigateTo({
        url: '/pages/studentNotes/noteList/noteList?id=' + this.data.sId + "&type=" + e.currentTarget.dataset.type
      })
    } else if (e.currentTarget.dataset.type == "STUDENT_TO_SCHOOL") {
      wx.navigateTo({
        url: '/pages/studentNotes/noteList/noteList?id=' + e.currentTarget.dataset.id + "&type=" + e.currentTarget.dataset.type
      })
    }
  }
})