import API from '../../../api/api';

Page({
  data: {
    nId: "",
    detailData: {},

    showViewComment: "none",
    focus: false,
    isLoading: false,
    isNetOff: false,
    txtContent: "",
    txtWidth: "0px"
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          txtWidth: (res.windowWidth - 38) + "px"
        });
      }
    });
    this.setData({ nId: options.nId }, function () {
      that.refreshData(that.data.nId, true);
    });
  },
  bindRefreshData: function(){
    this.refreshData(this.data.nId, true);
  },
  refreshData: function (nId, isLoading){
    var that = this;
    this.setData({ detailData: {}, isLoading: isLoading, isNetOff: false });
    
    API.findNoteDetail({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      nId: nId,
      success: function (json) {
        if (json.statusCode != 200) {
          that.setData({ isLoading: false, isNetOff: true });
          return;
        }
        if (json.data.code == 1) {
          var cd = new Date(json.data.data.note.createdOn);
          json.data.data.note.createdOn = cd.Format("MM-dd hh:mm:ss");

          if (json.data.data.note.lastModifiedOn != undefined) {
            var d = new Date(json.data.data.note.lastModifiedOn);
            json.data.data.note.lastModifiedOn = d.Format("MM-dd hh:mm:ss");
          }
          json.data.data.note.sender = json.data.data.note.sender.substr(0, 10) + "...";

          for (var i = 0; i < json.data.data.commentList.length; i++) {
            if (json.data.data.commentList[i].commentDate != undefined){
              var d = new Date(json.data.data.commentList[i].commentDate);
              json.data.data.commentList[i].commentDate = d.Format("MM-dd hh:mm:ss");
            }
          }
          that.setData({
            detailData: json.data.data,
            isLoading: false, isNetOff: false
          });
        }
      },
      fail: function (err) {
        that.setData({ isLoading: false, isNetOff: true });
        wx.showModal({
          content: "Please connect the network.",
          confirmText: 'OK',
          showCancel: false
        });
      }
    });
  },
  //解决 IOS 系统，提交无反应
  hideKeyboard: function () {
    wx.hideKeyboard();
  },
  showCommentView: function () {
    this.setData({ showViewComment: "block", focus: true });
  },
  bindFormReset: function () {
    this.setData({ showViewComment: "none", focus: false });
  },
  bindFormSubmit: function (e) {
    if (e.detail.value.content.trim() == "") {
      wx.showModal({
        content: 'The content of the comment cannot be empty.',
        confirmText: 'OK',
        showCancel: false
      })
      return;
    }
    wx.showToast({
      title: 'Sending...',
      icon: 'loading',
      duration: 10000,
      mask: true
    });
    var that = this;
    API.addCommment({
      method: "POST",
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      data: {
        nId: this.data.nId,
        source: 'Harmony App',
        content: e.detail.value.content
      },
      success: function (json) {
        if (json.data.code == 1) {
          that.setData({ showViewComment: "none", focus: false, txtContent: "" });
          that.refreshData(that.data.nId, false);
          wx.showToast({
            title: 'Success',
            icon: 'success',
            duration: 2000
          });
        }
      },
      fail: function (res) {
        wx.hideToast();
        wx.showModal({
          content: 'Please connect the network.',
          confirmText: 'OK',
          showCancel: false
        })
      }
    });
  },
  onUnload: function(){
    var pages = getCurrentPages();
    var prevPage = pages[pages.length - 2];  //上一个页面

    for (var i = 0; i < prevPage.data.serverData.length; i++) {
      if (prevPage.data.serverData[i].id == this.data.detailData.note.id){
        prevPage.data.serverData[i].commenterIds = this.data.detailData.note.commenterIds;
        break;
      }
    }

    prevPage.setData({
      serverData: prevPage.data.serverData
    })
  },
})