import API from '../../../api/api';

Page({
  data: {
    receiverId: '',
    emergencyImportant: false,
    notify: true,
    message: '',
    source: 'Harmony App',
    type: ''
  },
  onLoad: function (options) {
    this.setData({
      receiverId: options.receiverId,
      type: options.type
    });
  },
  bindInput: function(e){
    this.setData({ message: e.detail.value });
  },
  checkboxChange: function(e){
    var emergencyImportant = false; var notify = false;
    for (var i = 0; i < e.detail.value.length; i++) {
      if (e.detail.value[i] == "Important") {
        emergencyImportant = true;
      }
      if (e.detail.value[i] == "Email"){
        notify = true;
      }
    }
    
    this.setData({
      notify: notify,
      emergencyImportant: emergencyImportant
    });
  },
  bindSend: function(){
    var that = this;
    if (this.data.message.trim() == ""){
      wx.showModal({
        content: "The description cannot be empty.",
        confirmText: 'OK',
        showCancel: false
      });
      return;
    }

    wx.showLoading({ title: 'Sending...', mask: true });
    API.addNote({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      receiverId: this.data.receiverId,
      notify: this.data.notify, //重要的，加通知
      data:{
        message: this.data.message, 
        type: this.data.type, 
        source: this.data.source, 
        emergencyImportant: this.data.emergencyImportant
      },
      success: function (json) {
        if (json.statusCode != 200) {
          wx.hideLoading();
          wx.showModal({
            content: "Add note failed.",
            confirmText: 'OK',
            showCancel: false
          });
          return;
        }
        if (json.data.code == 1){
          wx.hideLoading();
          var pages = getCurrentPages();
          var prevPage = pages[pages.length - 2];  //上一个页面
          prevPage.onRefreshData(prevPage.data.receiverId, prevPage.data.type, prevPage.data.pageNumber, prevPage.data.pageSize);
          wx.navigateBack();
        }
      },
      fail: function (err) {
        wx.hideLoading();
        wx.showModal({
          content: "Please connect the network.",
          confirmText: 'OK',
          showCancel: false
        });
      }
    });
  }
})