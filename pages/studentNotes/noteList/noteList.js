import API from '../../../api/api';

Page({
  data: {
    receiverId: "",
    type: "",
    serverData: [],
    currentPage: 1,
    totalPage: 0,
    pageSize: 0,
    isLoading: false,
    isNetOff: false,
    viewHeight: '0px'
  },
  onLoad: function (options) {
    var that = this;
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          viewHeight: (res.windowHeight - 46) + "px"
        });
      }
    });
    this.setData({ receiverId: options.id, type: options.type }, function () {
      that.onRefreshData(that.data.receiverId, that.data.type, 1, 10);
    });
  },
  bindScrollToLower: function () {
    if (this.data.currentPage < this.data.totalPage) {
      this.data.currentPage++;
      this.onRefreshData(this.data.receiverId, this.data.type, 1, (this.data.currentPage * 10));
    }
  },
  onRefreshData: function (receiverId, type, pageNumber, pageSize) {
    var that = this;

    var paramData = {
      conditions: {
        receiverId: receiverId,
        type: type,
        keyword: ""
      },
      pageNumber: pageNumber,
      pageSize: pageSize,
      dir: 'DESC',
      sort: 'createdOn'
    };
    if (pageSize <= 10) {
      that.setData({ isLoading: true, isNetOff: false, pageSize: pageSize });
    } else {
      that.setData({ pageSize: pageSize });
    }
    API.findNotes({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      data: paramData,
      success: function (json) {
        if (json.statusCode != 200) {
          that.setData({ isLoading: false, isNetOff: true, totalPage: 0, currentPage: that.data.currentPage });
          return;
        }
        if (json.data.code == 1){
          for (var i = 0; i < json.data.data.content.length;i++){
            var cd = new Date(json.data.data.content[i].createdOn);
            json.data.data.content[i].createdOn = cd.Format("MM-dd hh:mm:ss");

            if (json.data.data.content[i].lastModifiedOn != undefined) {
              var d = new Date(json.data.data.content[i].lastModifiedOn);
              json.data.data.content[i].lastModifiedOn = d.Format("MM-dd hh:mm:ss");
            }
            json.data.data.content[i].sender = json.data.data.content[i].sender.substr(0,10) + "...";
          }

          if (pageSize <= 10) {
            that.setData({ serverData: json.data.data.content, totalPage: json.data.data.totalPage, isLoading: false, isNetOff: false, currentPage: that.data.currentPage });
          } else {
            that.setData({ serverData: json.data.data.content, isLoading: false, isNetOff: false, currentPage: that.data.currentPage });
          }
        }else{
          that.setData({ isLoading: false, isNetOff: true });
        }
      },
      fail: function (err) {
        that.data.currentPage--;
        that.setData({ isLoading: false, isNetOff: true });
      }
    });
  },
  bindRefreshData: function () {
    this.onRefreshData(this.data.receiverId, this.data.type, this.data.pageNumber, this.data.pageSize);
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
    this.onRefreshData(this.data.receiverId, this.data.type, this.data.pageNumber, this.data.pageSize);
  },
  gotoNoteDetails: function(e){
    wx.navigateTo({
      url: '/pages/studentNotes/noteDetail/noteDetail?nId=' + e.currentTarget.dataset.id
    })
  },
  bindAddNote: function(){
    wx.navigateTo({
      url: '/pages/studentNotes/addNote/addNote?receiverId=' + this.data.receiverId + "&type=" + this.data.type
    })
  }
})