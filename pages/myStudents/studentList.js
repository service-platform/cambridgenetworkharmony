import API from '../../api/api';
import utils from '../../utils/util.js';

Page({
  data: {
    timeout: 0,
    currentPage: 1,
    totalPage: 0,
    pageSize: 0,
    serverData: [],
    studentName: '',
    viewHeight: '0px',
    marginTop: '0px',
    viewWidth: '272px',
    isLoading: false,
    isNetOff: false
  },
  onLoad: function () {
    //获取设备的 高度
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          viewHeight: (res.windowHeight - 41) + "px",
          viewWidth: (res.windowWidth - 103) + "px",
          marginTop: parseInt((res.windowHeight - 90) / 2) + "px"
        });
      }
    });
    this.refreshData(1, 10);
  },
  bindInput: function (e) {
    var that = this;
    if (e.target.dataset.flag == "studentName") {
      this.setData({ studentName: e.detail.value });

      clearTimeout(that.data.timeout);
      that.data.timeout = setTimeout(function () {
        that.refreshData(1, 10);
      }, 1500);
    } else if (e.target.dataset.flag == "description") {
      this.setData({ description: e.detail.value });
    }
  },
  bindScrollToLower: function () {
    if (this.data.currentPage < this.data.totalPage) {
      this.data.currentPage++;
      this.refreshData(1, (this.data.currentPage * 10));
    }
  },
  bindRefreshData: function(){
    this.refreshData(1, 10);
  },
  gotoStudentDetails: function(e){
    wx.navigateTo({
      url: '/pages/myStudents/studentDetails/studentDetails?sId=' + e.currentTarget.dataset.sid + "&item=" + encodeURIComponent(JSON.stringify(e.currentTarget.dataset.item)) + "&englishName=" + encodeURIComponent((e.currentTarget.dataset.englishname) ? e.currentTarget.dataset.englishname : "")
    })
  },
  // conditions: {
  //   studentName: this.data.studentName,
  //   studentId: '',
  //   schoolName: ''
  // },
  refreshData: function (pageNumber, pageSize) {
    var that = this;
    var conditions = {};
    if (this.data.studentName.trim() == ''){
        delete conditions.studentName;
    }else{
        conditions.studentName = this.data.studentName;
    }
    var paramData = {
      conditions: conditions,
      pageNumber: pageNumber,
      pageSize: pageSize
    };
    if (pageSize <= 10){
      that.setData({ isLoading: true, isNetOff: false, pageSize: pageSize});
    }else{
      that.setData({ pageSize: pageSize });
    }
    API.studentList({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      data: paramData,
      success: function (json) {
        if (json.statusCode != 200) {
          that.setData({ isLoading: false, isNetOff: true, serverData: [], totalPage: 0, currentPage: that.data.currentPage });
          return;
        }
        if (json.data.code == 1){
          if (pageSize <= 10){
            that.setData({ serverData: json.data.data.content, totalPage: json.data.data.totalPage, isLoading: false, isNetOff: false, currentPage: that.data.currentPage });
          }else{
            that.setData({ serverData: json.data.data.content, isLoading: false, isNetOff: false, currentPage: that.data.currentPage});
          }
        }
      },
      fail: function (err) {
        that.data.currentPage--;
        that.setData({ isLoading: false, isNetOff: true, currentPage: that.data.currentPage});
      }
    })
  },
  gotoPhotoAlbum: function(e){
    wx.navigateTo({
      url: '/pages/photoAlbum/photoAlbum?sid=' + e.target.dataset.sid
    })
  },
  gotoStudentNotes: function(e){
    wx.navigateTo({
      url: '/pages/studentNotes/studentNotes?sId=' + e.target.dataset.sid + "&englishName=" + encodeURIComponent(e.target.dataset.englishname)
    })
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})