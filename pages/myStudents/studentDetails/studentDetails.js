import API from '../../../api/api';
import utils from '../../../utils/util.js';

Page({
  data: {
    serverData: {},
    isError: false,
    sId: "",
    items: [],
    viewWidth: "204px",
    downloadMarginTop: '24px'
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        if (res.platform == "android") {
          that.setData({ viewWidth: (res.windowWidth - 124) + "px", downloadMarginTop: '0px' });
        } else if (res.platform == "ios") {
          that.setData({ viewWidth: (res.windowWidth - 124) + "px", downloadMarginTop: '-5px' });
        } else {
          that.setData({ viewWidth: (res.windowWidth - 124) + "px", downloadMarginTop: '0px' });
        }
      }
    });
    var items = JSON.parse(decodeURIComponent(options.item));
    
    this.setData({ sId: options.sId, items: items, englishName: decodeURIComponent(options.englishName) }, function () {
      that.refreshData(that.data.sId);
    });
  },
  makePhoneCall: function(e){
    if (e.currentTarget.dataset.phone != undefined){
      wx.makePhoneCall({ phoneNumber: e.currentTarget.dataset.phone });
    }
  },
  bindRefreshData: function(){
    this.refreshData(this.data.sId);
  },
  refreshData: function(sId){
    var that = this;
    this.setData({ isError: false });
    API.findStuById({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      sId: sId,
      success: function (json) {
        if (json.statusCode != 200){
          that.setData({ isError: true});
          return;
        }
        if (json.data.code == 1) {
          var year = parseInt(json.data.data.student.passportInfo.dateOfBirth.split('-')[0]);
          var currentYear = new Date().getFullYear();
          json.data.data.student.passportInfo.age = (currentYear - year);

          //排除自己的信息
          var newArrays = [];
          for (var i = 0; i < json.data.data.studentContact.length; i++) {
            json.data.data.studentContact[i].relationShip = that.replaceStr(json.data.data.studentContact[i].relationShip);
            if (json.data.data.studentContact[i].relationShip != "Myself" && json.data.data.studentContact[i].relationShip != "Agent") {
              newArrays.push(json.data.data.studentContact[i]);
            }
          }
          json.data.data.studentContact = newArrays;

          if (json.data.data.summary.studentDetail.residentialChoice != undefined) {
            json.data.data.summary.studentDetail.residentialChoice = json.data.data.summary.studentDetail.residentialChoice.replace(/_/g, " ");
            json.data.data.summary.studentDetail.residentialChoice = that.replaceStr(json.data.data.summary.studentDetail.residentialChoice);
          }
          json.data.data.initialFlag = false;
          json.data.data.residentialFlag = false;
          json.data.data.academicFlag = false;
          json.data.data.eslFlag = false;
          if (json.data.data.student.fullName != "") {
            wx.setNavigationBarTitle({ title: json.data.data.student.fullName });
          }
          if (json.data.data.summary.studentDetail.schoolName != undefined){
            json.data.data.summary.studentDetail.schoolName = json.data.data.summary.studentDetail.schoolName.split('(')[0];
          }else{
            json.data.data.summary.studentDetail.schoolName = "NA";
          }
          that.setData({
            serverData: json.data.data,
            isError: false
          }, function () {
            that.getFEAOrSSC('SCHOOL', that.data.serverData.summary.studentDetail.schoolId, 'FEA');
          });
          console.log(json.data.data);
        }
      },
      fail: function (err) {
        that.setData({ isError: true });
        wx.showModal({
          content: 'Please connect the network',
          confirmText: 'OK',
          showCancel: false
        });
      }
    })
  },
  getFEAOrSSC: function (entityType, entityId, title){
    var that = this;
    API.getFEAAndSSC({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      entityType: entityType,
      entityId: entityId,
      title: title,
      success: function (json) {
        if (json.data.code == 1) {
          if (title == "FEA"){
            that.data.serverData.FEA = json.data.data;
            that.setData({
              serverData: that.data.serverData
            }, function () {
              that.getFEAOrSSC('SCHOOL', entityId, 'SSC');
            });
          }else{
            that.data.serverData.SSC = json.data.data;
            that.setData({
              serverData: that.data.serverData
            });
          }
        }
      },
      fail: function (err) {
        console.log(err);
      }
    });
  },
  selectGroupItem: function (e) {
    if (e.target.dataset.type == "initial") {
      if (this.data.serverData.initialFlag == true) {
        this.setData({
          serverData: {
            ...this.data.serverData,
            initialFlag: false
          }
        });
      } else {
        this.reportList(e.target.dataset.type);
      }
    } else if (e.target.dataset.type == "residential") {
      if (this.data.serverData.residentialFlag == true) {
        this.setData({
          serverData: {
            ...this.data.serverData,
            residentialFlag: false
          }
        });
      } else {
        this.reportList(e.target.dataset.type);
      }
    } else if (e.target.dataset.type == "academic") {
      if (this.data.serverData.academicFlag == true) {
        this.setData({
          serverData: {
            ...this.data.serverData,
            academicFlag: false
          }
        });
      } else {
        this.reportList(e.target.dataset.type);
      }
    } else if (e.target.dataset.type == "esl") {
      if (this.data.serverData.eslFlag == true) {
        this.setData({
          serverData: {
            ...this.data.serverData,
            eslFlag: false
          }
        });
      } else {
        this.reportList(e.target.dataset.type);
      }
    }
  },
  reportList: function (type){
    //initial、residential、academic、esl
    var that = this;
    if (that.data.serverData.student != undefined){
      wx.showLoading({ title: 'Loading...', mask: true });
      API.reports({
        method: 'POST',
        header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("userId"),
          wechatAuth: wx.getStorageSync("openId")
        },
        studentId: that.data.serverData.student.id,
        type: type,
        success: function (json) {
          if (json.statusCode != 200){
            wx.hideLoading();
            wx.showModal({
              content: 'Please connect the network',
              confirmText: 'OK',
              showCancel: false
            });
            return;
          }
          if (json.data.data == "No report to display."){
            switch (type) {
              case "initial":
                that.data.serverData.initialFlag = true;
                that.data.serverData.initialList = [];
                break;
              case "residential":
                that.data.serverData.residentialFlag = true;
                that.data.serverData.residentialList = [];
                break;
              case "academic":
                that.data.serverData.academicFlag = true;
                that.data.serverData.academicList = [];
                break;
              case "esl":
                that.data.serverData.eslFlag = true;
                that.data.serverData.eslList = [];
                break;
            }
            that.setData({
              serverData: that.data.serverData
            });
          }else{
            var serverData = json.data.data;
            for (var i = 0; i < serverData.d.length; i++) {
              if (serverData.d[i].originalReportDate != undefined){
                serverData.d[i].reportDateEn = that.convertUTCTimeToLocalTime(serverData.d[i].originalReportDate)
              }
              if (serverData.d[i].schoolLogo != undefined && serverData.d[i].schoolLogo.indexOf("s=null") != -1) {
                serverData.d[i].schoolLogo = serverData.d[i].schoolLogo.replace("s=null", "s=" + API.fileSecret);
              }
              if (serverData.d[i].docURL != undefined && serverData.d[i].docURL.indexOf("s=null") != -1) {
                serverData.d[i].docURL = serverData.d[i].docURL.replace("s=null", "s=" + API.fileSecret);
              }
            }

            if (json.data.code == 1) {
              switch (type) {
                case "initial":
                  that.data.serverData.initialFlag = true;
                  if (serverData.status == "error") {
                    that.data.serverData.initialList = [];
                  } else {
                    that.data.serverData.initialList = serverData.d;
                  }
                  break;
                case "residential":
                  that.data.serverData.residentialFlag = true;
                  if (serverData.status == "error") {
                    that.data.serverData.residentialList = [];
                  } else {
                    that.data.serverData.residentialList = serverData.d;
                  }
                  break;
                case "academic":
                  that.data.serverData.academicFlag = true;
                  if (serverData.status == "error") {
                    that.data.serverData.academicList = [];
                  } else {
                    that.data.serverData.academicList = serverData.d;
                  }
                  break;
                case "esl":
                  that.data.serverData.eslFlag = true;
                  if (serverData.status == "error") {
                    that.data.serverData.eslList = [];
                  } else {
                    that.data.serverData.eslList = serverData.d;
                  }
                  break;
              }
              that.setData({
                serverData: that.data.serverData
              });
            }
          }
          
          console.log(serverData);
          wx.hideLoading();
        },
        fail: function (err) {
          console.log(err);
          wx.hideLoading();
        }
      });
    }
  },
  openDetails: function (e) {
    wx.navigateTo({
      url: '/pages/reportDetail/details?reportId=' + e.currentTarget.dataset.reportid + "&type=" + e.currentTarget.dataset.type + "&studentId=" + this.data.serverData.student.id + "&docURL=" + ((e.currentTarget.dataset.docurl == undefined) ? "" : e.currentTarget.dataset.docurl.replace("?s=", "&s="))
    });
  },
  replaceStr: function(str){ // 正则法
     str = str.toLowerCase();
     var reg = /\b(\w)|\s(\w)/g; //  \b判断边界\s判断空格
     return str.replace(reg, function (m) {
        return m.toUpperCase()
     });
  },
  getExtensionName: function (str) {
    var index = str.indexOf("?s=");
    var tempStr = str.substr(0, index);
    var returnStr = tempStr.substr(tempStr.lastIndexOf("."), tempStr.length);
    return returnStr;
  },
  openTranslateDocument: function (e) {
    var that = this; var tempDataset = e.currentTarget.dataset;
    if (e.currentTarget.dataset.url == undefined || e.currentTarget.dataset.url.trim() == "") {
      wx.showModal({
        content: 'No translation report',
        confirmText: 'OK',
        showCancel: false
      });
    } else {
      wx.getNetworkType({
        success: function (res) {
          // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
          var networkType = res.networkType;
          if (networkType == "none") {
            wx.showModal({
              content: 'No network at present, retry after link',
              confirmText: 'OK',
              showCancel: false
            });
            return;
          } else if (networkType == "wifi") {
            wx.showLoading({ title: 'Downloading', mask: true });

            var extensionName = that.getExtensionName(e.currentTarget.dataset.url);
            console.log(e.currentTarget.dataset.url + "&a=" + extensionName);
            wx.downloadFile({
              url: e.currentTarget.dataset.url + "&a=" + extensionName,
              success: function (res) {
                var filePath = res.tempFilePath;
                wx.showLoading({ title: 'Open...', mask: true });

                if (filePath.indexOf(".png") != -1 || filePath.indexOf(".gif") != -1 || filePath.indexOf(".jpg") != -1) {
                  that.setData({
                    imgSrc: filePath
                  });
                } else {
                  wx.openDocument({
                    filePath: filePath,
                    success: function (res) {
                      wx.hideLoading();
                      console.log('打开文档成功');
                    },
                    fail: function (err) {
                      wx.hideLoading();
                      wx.showModal({
                        content: err.errMsg,
                        confirmText: 'OK',
                        showCancel: false
                      });
                    }
                  });
                }
              },
              fail: function (err) {
                wx.hideLoading();
                wx.showModal({
                  content: 'Please connect the network',
                  confirmText: 'OK',
                  showCancel: false
                });
              }
            })
          } else if (networkType == "2g" || networkType == "3g" || networkType == "4g") {
            wx.showModal({
              content: 'Currently using a cellular network, would you like to continue downloading?',
              success: function (res) {
                if (res.confirm) {
                  wx.showLoading({ title: 'Downloading', mask: true });

                  var extensionName = that.getExtensionName(e.currentTarget.dataset.url);
                  wx.downloadFile({
                    url: e.currentTarget.dataset.url + "&a=" + extensionName,
                    success: function (res) {
                      var filePath = res.tempFilePath;
                      wx.showLoading({ title: 'Open...', mask: true });

                      if (filePath.indexOf(".png") != -1 || filePath.indexOf(".gif") != -1 || filePath.indexOf(".jpg") != -1) {
                        that.setData({
                          imgSrc: filePath
                        });
                      } else {
                        wx.openDocument({
                          filePath: filePath,
                          success: function (res) {
                            wx.hideLoading();
                            console.log('打开文档成功');
                          },
                          fail: function (err) {
                            wx.hideLoading();
                            wx.showModal({
                              content: err.errMsg,
                              confirmText: 'OK',
                              showCancel: false
                            });
                          }
                        });
                      }
                    },
                    fail: function (err) {
                      wx.hideLoading();
                      wx.showModal({
                        content: 'Please connect the network',
                        confirmText: 'OK',
                        showCancel: false
                      });
                    }
                  })
                }
              }
            });
          }
        }
      });
    }
  },
  convertUTCTimeToLocalTime: function (UTCDateString) {
    if (!UTCDateString) {
      return '-';
    }
    function formatFunc(str) {    //格式化显示
      return str > 9 ? str : '0' + str
    }
    var date2 = new Date(UTCDateString);     //这步是关键
    var year = date2.getFullYear();
    var mon = formatFunc(date2.getMonth() + 1);
    var day = formatFunc(date2.getDate());

    var dateStr = mon + '/' + day + "/" + year;
    return dateStr;
  },
  gotoPhotoAlbum: function(e){
    if (e.target.dataset.sid != undefined){
      wx.navigateTo({
        url: '/pages/photoAlbum/photoAlbum?sid=' + e.target.dataset.sid
      })
    }
  },
  gotoStudentNotes: function (e) {
    if (e.target.dataset.sid != undefined) {
      wx.navigateTo({
        url: '/pages/studentNotes/studentNotes?sId=' + e.target.dataset.sid + "&englishName=" + encodeURIComponent(e.target.dataset.englishname)
      })
    }
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})