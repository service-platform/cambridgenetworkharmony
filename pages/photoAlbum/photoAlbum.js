import API from '../../api/api';
import utils from '../../utils/util.js';

Page({
  data: {
    sId: '',
    originalData: [],
    serverData: [],
    selectVals: [],
    currentPage: 1,
    totalPage: 0,
    pageSize: 0,
    isLoading: false,
    isNetOff: false,
    imgWidth: '0px',
    scrollHeight: "0px",
    previewHeight: "0px"
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          imgWidth: Math.floor((res.windowWidth - 24) / 3) + "px",
          scrollHeight: (res.windowHeight - 53) + "px"
        });
      }
    })
    that.setData({ sId: options.sid}, function(){
      that.onRefreshData(that.data.sId, 1, (that.data.currentPage * 20));
    });
  },
  bindRefreshDeleteData: function(){
    this.onRefreshData(this.data.sId, 1, (this.data.currentPage * 20));
  },
  onPullDownRefresh: function(){
    wx.stopPullDownRefresh();
    this.onRefreshData(this.data.sId, 1, (this.data.currentPage * 20));
  },
  bindRefreshData: function(){
    this.onRefreshData(this.data.sId, 1, 20);
  },
  bindScrollToLower: function(){
    if (this.data.currentPage < this.data.totalPage) {
      this.data.currentPage++;
      this.onRefreshData(this.data.sId, 1, (this.data.currentPage * 20));
    }
  },
  onRefreshData: function (sId, pageNumber, pageSize){
    var that = this;
    if (pageSize <= 20) {
      that.setData({ isLoading: true, isNetOff: false, pageSize: pageSize });
    } else {
      that.setData({ pageSize: pageSize });
    }
    API.findGeneralPhotoesBySId({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      sid: sId,
      data: {
        conditions: {},
        pageNumber: pageNumber,
        pageSize: pageSize,
        dir: 'DESC'
      },
      success: function (json) {
        if (json.statusCode != 200) {
          that.setData({ isLoading: false, isNetOff: true, serverData: [], totalPage: 0, currentPage: that.data.currentPage });
          return;
        }
        if (json.data.code == 1){
          var tempArray = [];
          for (var i = 0; i < json.data.data.content.length; i++){
            var obj = json.data.data.content[i];
            if(tempArray.length <= 0){
                var objItem = {
                  date: new Date(obj.dateCreated).Format("MM-dd-yyyy"),
                  items: new Array(obj)
                };
                tempArray.push(objItem);
            }else{
                var haveData = false;
                for (var j = 0; j < tempArray.length; j++){
                  if (new Date(obj.dateCreated).Format("MM-dd-yyyy") == tempArray[j].date){
                    tempArray[j].items.push(obj);
                    haveData = true; break;
                  }
                }
                if (haveData == false){
                    var objItem = {
                      date: new Date(obj.dateCreated).Format("MM-dd-yyyy"),
                      items: new Array(obj)
                    };
                    tempArray.push(objItem);
                }
            }
          }
          if (pageSize <= 20) {
            that.setData({ serverData: tempArray, totalPage: json.data.data.totalPage, originalData: json.data.data.content, isLoading: false, isNetOff: false, currentPage: that.data.currentPage});
          }else{
            that.setData({ serverData: tempArray, originalData: json.data.data.content, isLoading: false, isNetOff: false, currentPage: that.data.currentPage });
          }
          console.log(tempArray);
        }
      },
      fail: function (err) {
        if (that.data.currentPage >= 2) { that.data.currentPage--; }
        that.setData({ isLoading: false, isNetOff: true, currentPage: that.data.currentPage });
      }
    })
  },
  showDescription: function(e){
    if (e.target.dataset.val != ""){
      wx.showModal({
        content: e.target.dataset.val,
        confirmText: 'OK',
        showCancel: false
      });
    }
  },
  openImage: function(e){
    if (e.currentTarget.dataset.img != "/images/imgError.png"){
      if (e.currentTarget.dataset.whethersharetoparent == true && e.currentTarget.dataset.whethersee == true){
        wx.navigateTo({
          url: '/pages/photoAlbum/previewImage/previewImage?img=' + encodeURIComponent(e.currentTarget.dataset.img) + "&desc=" + encodeURIComponent(e.currentTarget.dataset.description) + "&date=" + encodeURIComponent(e.currentTarget.dataset.date) + "&shareDate=" + encodeURIComponent(e.currentTarget.dataset.sharetoparenttime) + "&whethersharetoparent=" + e.currentTarget.dataset.whethersharetoparent  + "&sId=" + this.data.sId + "&nDId=" + e.currentTarget.dataset.ndid + "&btnDisabled=true&mask=student"
        })
      }else{
        wx.navigateTo({
          url: '/pages/photoAlbum/previewImage/previewImage?img=' + encodeURIComponent(e.currentTarget.dataset.img) + "&desc=" + encodeURIComponent(e.currentTarget.dataset.description) + "&date=" + encodeURIComponent(e.currentTarget.dataset.date) + "&shareDate=" + encodeURIComponent(e.currentTarget.dataset.sharetoparenttime) + "&whethersharetoparent=" + e.currentTarget.dataset.whethersharetoparent + "&sId=" + this.data.sId + "&nDId=" + e.currentTarget.dataset.ndid + "&btnDisabled=false&mask=student"
        })
      }
    }
  },
  eventsBlocked: function(){
    return; //事件阻塞，空方法
  },
  bindChange: function(e){
    this.setData({
      selectVals: e.detail.value
    });
  },
  btnShareToParent: function(){
    if (this.data.selectVals.length <= 0){
      wx.showModal({
        content: 'Please choose at least 1 photo.',
        confirmText: 'OK',
        showCancel: false
      });
      return;
    }
    var that = this; wx.showLoading({ title: 'Sharing...', mask: true });
    API.sharePhotoToParents({
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      data: this.data.selectVals,
      success: function (json) {
        wx.hideLoading();
        if (json.data.code == 1) {
          wx.showModal({
            content: "Share success",
            confirmText: 'OK',
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                that.onRefreshData(that.data.sId, 1, (that.data.currentPage * 20));
              }
            }
          });
        }
      },
      fail: function (err) {
        wx.hideLoading();
        wx.showModal({
          content: "Please connect the network.",
          confirmText: 'OK',
          showCancel: false
        });
      }
    });
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
