import API from '../../../api/api.js';
import utils from '../../../utils/util.js';

Page({
  data: {
    sId: "",
    nDId: "",
    mask: "",
    previewHeight: "0px",
    previewImage: "",
    description: "",
    date: "",
    shareDate: "",
    userName: "",
    btnDisabled: true,
    whetherShareToParent: false
  },
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          previewHeight: (res.windowHeight) + "px"
        });
      }
    });
    
    this.setData({
      sId: options.sId,
      nDId: options.nDId,
      mask: options.mask,
      previewImage: decodeURIComponent(options.img),
      description: decodeURIComponent(options.desc),
      date: new Date(decodeURIComponent(options.date)).Format("yyyy-MM-dd hh:mm:ss"),
      shareDate: new Date(decodeURIComponent(options.shareDate)).Format("yyyy-MM-dd hh:mm:ss"),
      btnDisabled: ((options.btnDisabled == "true")?true:false),
      whetherShareToParent: ((options.whethersharetoparent == "true")?true:false)
    }, function(){
      if (that.data.whetherShareToParent == true){
          API.generalPhotoDetail({
            method: 'GET',
            header: {
              'Content-Type': 'application/json',
              userAuth: wx.getStorageSync("userId"),
              wechatAuth: wx.getStorageSync("openId")
            },
            id: that.data.nDId,
            success: function (json) {
              if (json.data.code == 1) {
                that.setData({
                  userName: json.data.data.userProfile.baseInfo.fullName
                });
              }
            },
            fail: function (err) {
              that.setData({
                userName: "加载失败"
              });
            }
          });
      }
    });
  },
  removeGeneralPhotoes: function(){
    if (this.data.btnDisabled == false){
      var that = this;
      wx.showModal({
        content: 'Are you sure you want to delete ?',
        confirmText: 'OK',
        cancelText: 'Cancel',
        success: function (res) {
          if (res.confirm) {
            wx.showLoading({ title: 'delete...', mask: true });

            if (that.data.mask == 'student'){
              API.removeGeneralPhotoes({
                method: 'POST',
                header: {
                  'Content-Type': 'application/json',
                  userAuth: wx.getStorageSync("userId"),
                  wechatAuth: wx.getStorageSync("openId")
                },
                data: {
                  sId: that.data.sId,
                  nDId: that.data.nDId
                },
                success: function (json) {
                  wx.hideLoading();
                  if (json.data.data.tOrf == true) {
                    wx.showModal({
                      content: json.data.data.msg,
                      confirmText: 'OK',
                      showCancel: false,
                      success: function (res) {
                        var pages = getCurrentPages();
                        var prevPage = pages[pages.length - 2];  //上一个页面
                        prevPage.bindRefreshDeleteData();
                        wx.navigateBack();
                      }
                    });
                  } else {
                    wx.showModal({
                      content: json.data.data.msg,
                      confirmText: 'OK',
                      showCancel: false
                    });
                  }
                },
                fail: function (err) {
                  wx.hideLoading();
                  wx.showModal({
                    content: 'Please connect the network.',
                    confirmText: 'OK',
                    showCancel: false
                  });
                }
              })
            }else{
              API.removeReportPhoto({
                method: 'POST',
                header: {
                  'Content-Type': 'application/json',
                  userAuth: wx.getStorageSync("userId"),
                  wechatAuth: wx.getStorageSync("openId")
                },
                data: {
                  newDId: that.data.nDId
                },
                success: function (json) {
                  wx.hideLoading();
                  if (json.data.data.tOrf == true) {
                    wx.showModal({
                      content: json.data.data.msg,
                      confirmText: 'OK',
                      showCancel: false,
                      success: function (res) {
                        var pages = getCurrentPages();
                        var prevPage = pages[pages.length - 2];  //上一个页面
                        prevPage.bindRefreshDeleteData();
                        wx.navigateBack();
                      }
                    });
                  } else {
                    wx.showModal({
                      content: json.data.data.msg,
                      confirmText: 'OK',
                      showCancel: false
                    });
                  }
                },
                fail: function (err) {
                  wx.hideLoading();
                  wx.showModal({
                    content: 'Please connect the network.',
                    confirmText: 'OK',
                    showCancel: false
                  });
                }
              })
            }
          }
        }
      });
    }
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
