import API from '../../api/api';
import utils from '../../utils/util.js';

Page({
  data: {
    serverData: {},
    docs: [],
    tempTypeText: "",
    docURL: "",
    enumDescription: [
      "Student did not attend, refuses to speak, and / or has never spoken in class.", //0
      "Student participates but only gives one word answers.Student never participates in class discussions and rarely answers teacher’s questions", //1
      "Student will answer teacher’s questions with simplistic answers. Student will occasionally participate in class discussions, but participation is minimal", //2
      "Student answers teacher’s questions adequately. Student participates adequately in class discussions. Student demonstrates some critical thinking ability", //3
      "Student always fully answers teacher’s questions and shows critical thinking skills. During class discussions, student will ask classmates questions and participate fully.", //4
      "Student fully answers student questions and responds with critical thinking. During class discussions, student will occasionally act as discussion leader and encourages critical thinking in others" //5
    ],
    reportId: "",
    studentId: ""
  },
  translateText: function (content){
    var str = '';
    if(content == 'INPROGRESS'){
        str = 'In Progress';
    }else if(content){
        var strArr = content.split('_');
        for(var i = 0 ; i < strArr.length;i++){
            strArr[i] = (strArr[i].substr(0,1)).toUpperCase() + (strArr[i].substr(1)).toLowerCase()
        }
        str = strArr.join(' ');
        content = str;
    }else{
        content = '';
    }
    return str;
  },
  getImageArrays: function(docs){
    var retArrays = new Array();
    for(var i = 0;i< docs.length;i++){
        var index = docs[i].indexOf("?s=");
        var tempStr = docs[i].substr(0, index);
        var extensionName = tempStr.substr(tempStr.lastIndexOf("."), tempStr.length).toLowerCase();

        if(extensionName == ".png" || extensionName == ".jpg" || extensionName == ".gif"|| extensionName == ".bmp"){
            retArrays.push(docs[i]);
        }
    }
    return retArrays;
  },
  onLoad: function (e) {
      this.setData({ reportId: e.reportId, studentId: e.studentId });
      if (e.type == "initial"){
        wx.setNavigationBarTitle({ title: 'Initlal Wellness Report' });
      } else if (e.type == "residential") {
        wx.setNavigationBarTitle({ title: 'Residentail Progress Report' });
      } else if (e.type == "academic") {
        wx.setNavigationBarTitle({ title: 'School Academic Progress Report' });
      } else if (e.type == "esl") {
        wx.setNavigationBarTitle({ title: 'Cambridge Academic Service Report' });
      }
      var that = this; this.setData({ tempTypeText: e.type, docURL: ((e.docURL == "")?"":e.docURL + "?s=" + e.s)});
      wx.showLoading({
            title: '',
            icon: 'loading',
            mask:true
      });
      API.reportDetail({
          method:'POST',
          reportId: e.reportId,
          type: e.type,
          studentId: e.studentId,
          header: {
            'Content-Type': 'application/json',
            userAuth: wx.getStorageSync("userId"),
            wechatAuth: wx.getStorageSync("openId")
          },
          success:function(json){
              //Initial、Residential、Academic
              json.data = json.data.data.d;
              if(json.data.status == "error"){
                wx.showModal({
                    content: json.data.errmsg,
                    showCancel: false
                })
              }else{
                if(that.data.tempTypeText == "academic"){
                    json.data.detail.type = that.translateText(json.data.detail.type);
                }
                if(that.data.tempTypeText == "residential" && json.data.detail.type == "FULL"){
                    if(json.data.detail != undefined){
                      json.data.detail.reportFor = json.data.detail.full.month;
                      json.data.detail.full.today = new Date(json.data.detail.full.today).Format("MM/dd/yyyy");
                      
                      if (json.data.detail.readOnly.submittedDate != undefined){
                        json.data.detail.readOnly.submittedDate = new Date(json.data.detail.readOnly.submittedDate).Format("MM/dd/yyyy");
                      }
                      if (json.data.detail.readOnly.translatedDate != undefined) {
                        json.data.detail.readOnly.translatedDate = new Date(json.data.detail.readOnly.translatedDate).Format("MM/dd/yyyy");
                      }
                    }
                } else if (that.data.tempTypeText == "residential" && json.data.detail.type == "CHECK_IN") {
                    if (json.data.detail != undefined) {
                      json.data.detail.reportFor = json.data.detail.checkIn.month;
                      json.data.detail.checkIn.today = new Date(json.data.detail.checkIn.today).Format("MM/dd/yyyy");

                      if (json.data.detail.readOnly.submittedDate != undefined) {
                        json.data.detail.readOnly.submittedDate = new Date(json.data.detail.readOnly.submittedDate).Format("MM/dd/yyyy");
                      }
                      if (json.data.detail.readOnly.translatedDate != undefined) {
                        json.data.detail.readOnly.translatedDate = new Date(json.data.detail.readOnly.translatedDate).Format("MM/dd/yyyy");
                      }
                    }
                }
                if (that.data.tempTypeText == "esl" && json.data.detail.type == "FULL") {
                  if (json.data.detail != undefined) {
                    json.data.detail.reportFor = json.data.detail.full.month;

                    if (json.data.detail.full.from != undefined) {
                      json.data.detail.full.from = new Date(json.data.detail.full.from).Format("MM-dd-yyyy");
                    }
                    if (json.data.detail.full.to != undefined) {
                      json.data.detail.full.to = new Date(json.data.detail.full.to).Format("MM-dd-yyyy");
                    }
                  }
                } else if (that.data.tempTypeText == "esl" && json.data.detail.type == "CHECK_IN") {
                  if (json.data.detail != undefined) {
                    json.data.detail.reportFor = json.data.detail.checkIn.month;

                    if (json.data.detail.checkIn.from != undefined) {
                      json.data.detail.checkIn.from = new Date(json.data.detail.checkIn.from).Format("MM-dd-yyyy");
                    }
                    if (json.data.detail.checkIn.to != undefined) {
                      json.data.detail.checkIn.to = new Date(json.data.detail.checkIn.to).Format("MM-dd-yyyy");
                    }
                    if (json.data.detail.checkIn.rankParticipation != undefined) {
                      json.data.detail.checkIn.enumDescription = " - " + that.data.enumDescription[json.data.detail.checkIn.rankParticipation];
                    }
                  }
                }
                for (var i = 0; i < json.data.docs.length; i++) {
                  if (json.data.docs[i].indexOf("s=null") != -1) {
                    json.data.docs[i] = json.data.docs[i].replace("s=null", "s=" + API.fileSecret);
                  }
                }
                json.data.detail.imageArrays = that.getImageArrays(json.data.docs);
                if (json.data.originalDocUrl != undefined){
                  json.data.detail.originalDocUrl = json.data.originalDocUrl;
                }
                if (json.data.translatedDocUrl != undefined) {
                  json.data.detail.translatedDocUrl = json.data.translatedDocUrl;
                }
                
                if (that.data.tempTypeText != "academic") {
                  json.data.detail.tempDocURL = that.data.docURL;
                  that.setData({
                    serverData: json.data.detail,
                    docs: json.data.docs
                  });
                }else{
                  that.setData({
                    serverData: json.data.detail,
                    docs: json.data.docs
                  });
                }
              }
              wx.hideLoading();
          },
          fail:function(res){
              wx.showModal({
                content: 'Please connect the network',
                showCancel: false
              });
          }
      });
  },
  getExtensionName: function (str) {
    var index = str.indexOf("?s=");
    var tempStr = str.substr(0, index);
    var returnStr = tempStr.substr(tempStr.lastIndexOf("."), tempStr.length);
    return returnStr;
  },
  openTranslateDocument: function (e) {
    var that = this; var tempDataset = e.currentTarget.dataset;
    if (e.currentTarget.dataset.url == undefined || e.currentTarget.dataset.url.trim() == "") {
      var modalContent = (this.data.tempTypeText == 'academic') ? 'No school report' : 'No translation report';
      if (tempDataset.docflag == "Translated"){
        modalContent = 'No translation report';
      }
      wx.showModal({
        content: modalContent,
        showCancel: false
      });
    } else {
      wx.getNetworkType({
        success: function (res) {
          // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
          var networkType = res.networkType;
          if (networkType == "none") {
            wx.showModal({
              content: 'No network at present, retry after link',
              showCancel: false
            });
            return;
          } else if (networkType == "wifi") {
            wx.showLoading({
              title: 'Downloading',
              mask: true
            });

            var extensionName = that.getExtensionName(e.currentTarget.dataset.url);
            wx.downloadFile({
              url: e.currentTarget.dataset.url + "&a=" + extensionName,
              success: function (res) {
                var filePath = res.tempFilePath;
                wx.showLoading({
                  title: 'Open...',
                  mask: true
                });
                if (filePath.indexOf(".png") != -1 || filePath.indexOf(".gif") != -1 || filePath.indexOf(".jpg") != -1) {
                  that.setData({
                    imgSrc: filePath
                  });
                } else {
                  wx.openDocument({
                    filePath: filePath,
                    success: function (res) {
                      wx.hideLoading();
                      console.log('打开文档成功');
                    },
                    fail: function (err) {
                      wx.hideLoading()
                      wx.showModal({
                        content: err.errMsg,
                        showCancel: false
                      });
                    }
                  });
                }
              },
              fail: function (err) {
                wx.hideLoading()
                wx.showModal({
                  content: 'Please connect the network',
                  showCancel: false
                });
              }
            })
          } else if (networkType == "2g" || networkType == "3g" || networkType == "4g") {
            wx.showModal({
              content: 'Currently using a cellular network, would you like to continue downloading?',
              success: function (res) {
                if (res.confirm) {
                  wx.showLoading({
                    title: 'Downloading',
                    mask: true
                  });
                  var extensionName = that.getExtensionName(e.currentTarget.dataset.url);
                  wx.downloadFile({
                    url: e.currentTarget.dataset.url + "&a=" + extensionName,
                    success: function (res) {
                      var filePath = res.tempFilePath;
                      wx.showLoading({
                        title: 'Open...',
                        mask: true
                      });
                      if (filePath.indexOf(".png") != -1 || filePath.indexOf(".gif") != -1 || filePath.indexOf(".jpg") != -1) {
                        that.setData({
                          imgSrc: filePath
                        });
                      } else {
                        wx.openDocument({
                          filePath: filePath,
                          success: function (res) {
                            wx.hideLoading()
                            console.log('打开文档成功');
                          },
                          fail: function (err) {
                            wx.hideLoading()
                            wx.showModal({
                              content: err.errMsg,
                              showCancel: false
                            });
                          }
                        });
                      }
                    },
                    fail: function (err) {
                      wx.hideLoading()
                      wx.showModal({
                        content: 'Please connect the network',
                        showCancel: false
                      });
                    }
                  })
                }
              }
            });
          }
        }
      });
    }
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})
