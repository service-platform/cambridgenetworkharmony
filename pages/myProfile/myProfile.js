import API from '../../api/api';
import utils from '../../utils/util.js';

Page({
  data: {
    avatarUrl: '/images/login.png',
    nickName: 'Loading',
    imgWidth: '160px',
    xOffset: 2000
  },
  onLoad: function () {
    //获取设备的 宽度
    var that = this;
    this.whetherBind();
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          imgWidth: Math.floor((res.windowWidth - 51)/2) + "px",
          xOffset: (Math.floor(res.windowWidth / 2) - 23)
        });
      }
    });
  },
  findStaffByOpenId: function(){
    var that = this;
    API.findStaffByOpenId({
      method: "POST",
      header: {
        'Content-Type': 'application/json',
        userAuth: wx.getStorageSync("userId"),
        wechatAuth: wx.getStorageSync("openId")
      },
      success: function (json) {
        if (json.data.code == 1) {
          that.setData({
            avatarUrl: json.data.data.avatarUrl,
            nickName: json.data.data.nickName
          });
        }
      }
    });
  },
  whetherBind: function () {
    var that = this;
    wx.login({
      success: function (res) {
        if (res.code) {
          API.whetherBind({
            method: "GET",
            code: res.code,
            success: function (json) {
              if (json.statusCode == 200) {
                if (json.data.data.code == 0) {
                  wx.setStorageSync("openId", json.data.data.openId);
                  wx.redirectTo({
                    url: '/pages/login/login'
                  });
                } else {
                  wx.setStorageSync("openId", json.data.data.openId);
                  wx.setStorageSync("userId", json.data.data.userId);
                  that.findStaffByOpenId();
                }
              }
            }
          });
        }
      }
    });
  },
  gotoComingSoon: function(){
    wx.navigateTo({
      url: '/pages/comingSoon/index'
    });
  },
  gotoMySchool: function(){
    wx.navigateTo({
      url: '/pages/mySchool/mySchool'
    });
  },
  gotoMyStudents: function(){
    wx.navigateTo({
      url: '/pages/myStudents/studentList'
    });
  },
  btnSharePhoto: function () {
    wx.navigateTo({
      url: '/pages/sharePhoto/sharePhoto'
    })
  },
  bindUpdateProfile: function () {
    wx.navigateTo({
      url: '/pages/myProfile/updateProfile/updateProfile'
    })
  },
  formSubmitByFormId: function (e) {
    utils.sendFormIdAndGatherFormId(e);
  }
})