import API from '../../../api/api.js'
Page({
  data:{
    serverData: { avatarUrl: '/images/login.png'},
    isUpdate: false,
    viewHeight: "0px",
    switchServer: false, //true：中国，false 美国
    labelServer: 'America'
  },
  btnUpdateInfo: function(){
    this.setData({isUpdate: true});
  },
  formSubmit: function(e){
    //默认值
    if(e.detail.value.gender == ""){e.detail.value.gender = "1"}
    this.data.serverData.nickName = e.detail.value.nickName;
    this.data.serverData.gender = e.detail.value.gender;
    this.data.serverData.country = e.detail.value.country;
    this.data.serverData.province = e.detail.value.province;
    this.data.serverData.city = e.detail.value.city;
    var that = this;
    wx.showLoading({ title: 'Saving...', mask: true });
    API.updateStaffInfo({
      method: 'POST',
      header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("userId"),
          wechatAuth: wx.getStorageSync("openId")
      },
      data: this.data.serverData,
      success:function(data){
          if(data.data.code == 1){
              that.setData({isUpdate: false});
              that.refreshData();
              wx.showModal({
                content: 'Save successfully',
                confirmText: 'OK',
                showCancel: false
              });
          }
      },
      fail: function(){
          wx.hideLoading();
      }
    });
  },
  formReset: function(){
    this.setData({isUpdate: false});
  },
  onLoad:function(options){
    var that = this;

    this.setData({
      switchServer: wx.getStorageSync("countryFlag"), //true：中国，false 美国
      labelServer: wx.getStorageSync("countryFlag") ? 'China' :'America'
    },function(){
      that.refreshData();
    });

    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          viewHeight: res.windowHeight +"px"
        });
      }
    })
  },
  refreshData: function(){
    var that = this;
    wx.showLoading({ title: 'Loading', mask: true });
    API.findStaffByOpenId({
      method: 'POST',
      header: {
          'Content-Type': 'application/json',
          userAuth: wx.getStorageSync("userId"),
          wechatAuth: wx.getStorageSync("openId")
      },
      success:function(data){
        wx.hideLoading();
        that.setData({
          serverData: data.data.data
        });
        var pages = getCurrentPages();
        var prevPage = pages[pages.length - 2];  //上一个页面
        prevPage.setData({
          nickName: data.data.data.nickName,
          avatarUrl: data.data.data.avatarUrl
        })
      },
      fail: function () {
        wx.hideLoading();
      }
    });
  },
  selectServer: function(e){
    wx.setStorageSync("countryFlag", e.detail.value);
    this.setData({
      switchServer: wx.getStorageSync("countryFlag"), //true：中国，false 美国
      labelServer: wx.getStorageSync("countryFlag") ? 'China' : 'America'
    });
  }
})